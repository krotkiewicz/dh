from __future__ import with_statement
import os

from alembic import context
from sqlalchemy import engine_from_config, pool
from logging.config import fileConfig

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# HACK: we use venusian scanner to import all models, so they will be
# added to Base.metadata
import venusian
import dh
class FakeConfig(object):
    def with_package(self, *arg, **kwargs):
        return self

    def add_subscriber(self, *arg, **kwargs):
        return self

    def add_view(self, *args, **kwargs):
        return self
scanner = venusian.Scanner(config=FakeConfig())
scanner.scan(dh, ignore=['dh.testing', 'dh.tests'])

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
from dh.models.base import Base
target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def expandvars_dict(settings):
    """Expands all environment variables in a settings dictionary."""
    return {
        key: os.path.expandvars(value)
        for key, value in settings.iteritems()
    }


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    settings = config.get_section(config.config_ini_section)
    settings = expandvars_dict(settings)
    url = settings['sqlalchemy.url']
    context.configure(url=url, target_metadata=target_metadata)

    with context.begin_transaction():
        context.run_migrations()

def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    settings = config.get_section(config.config_ini_section)
    settings = expandvars_dict(settings)
    engine = engine_from_config(
                settings,
                prefix='sqlalchemy.',
                poolclass=pool.NullPool)

    connection = engine.connect()
    context.configure(
                connection=connection,
                target_metadata=target_metadata
                )

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()

