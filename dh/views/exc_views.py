import json

from formencode.api import Invalid
from pyramid.response import Response
from pyramid.httpexceptions import HTTPBadRequest, HTTPException
from pyramid.view import view_config
from dh.dhexc import APIError


@view_config(context=Invalid)
def formencode_error_view(exc, request):
    errors = exc.unpack_errors()
    data = {
        'code': 1001,
        'message': "Validation Error",
        'data': errors,
    }

    response = HTTPBadRequest(
        body=json.dumps(data),
        content_type='application/json',
    )
    return response

@view_config(context=HTTPException)
def httpexception_view(exc, request):
    exc.content_type = 'application/json'
    body = {
        'message': exc.explanation,
        'data': {
            'generic': exc.detail,
        },
    }
    if isinstance(exc, APIError):
        body['code'] = exc.dhcode

    exc.body = json.dumps(body)
    return exc
