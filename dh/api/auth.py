import uuid

import requests
from pyramid.httpexceptions import HTTPCreated, HTTPOk, HTTPNotFound
from pyramid.view import view_config
from pyramid.security import remember
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import or_

from dh import dhexc
from dh.models.user import User, Email, ActivationEmail, Auth, SocialInfo
from dh.models.debt import DebtHistory
from dh.utils.views import BaseAPI
from dh.utils.schema import schema
from dh.utils import mail, get_absolute_url
from . import schemas


@view_config(route_name='create_user', renderer='json')
class CreateUserAPI(BaseAPI):

    @schema(schemas.auth.CreateUserIn)
    def post(self, data):
        """
        Creates new user.
        Response 201 Created on success.
        """
        user, email = User.new(
            name=data['name'],
            email=data['email'],
            password=data['password'],
        )

        path = self.request.route_path(
            'activate_user',
            activation_code=email.activation,
        )
        activation_url = get_absolute_url(path)
        mail.send(mail.templates.ActivationEmail(
            user_email=email.email,
            user_name=user.name,
            activation_url=activation_url,
        ))
        return HTTPCreated()


class ActivateUserMixin(object):

    def delete_all_emails_and_all_users(self, activated_email):
        """
        After activation we have to remove all signs of the email.
        """
        self.DB.query(ActivationEmail) \
            .filter(ActivationEmail.email==activated_email).delete()

        users = self.DB.query(User) \
            .outerjoin(ActivationEmail) \
            .outerjoin(Email) \
            .filter(ActivationEmail.user_id==None) \
            .filter(Email.user_id==None)

        for u in users:
            self.DB.delete(u)

    def append_debts_to_user(self, activated_email, user):
        query = self.DB.query(DebtHistory)
        query = query.filter(or_(
            DebtHistory.took_id==None,
            DebtHistory.gave_id==None,
        ))
        query = query.filter(or_(
            DebtHistory.took_email==activated_email.email,
            DebtHistory.gave_email==activated_email.email,
        ))

        to_append_to_debt = []
        for hist in query:
            if not hist.took_id:
                hist.took_id = user.id
            else:
                hist.gave_id = user.id

            if hist.took_id == hist.gave_id:
                # son of a bitch !
                self.DB.delete(hist)
                continue

            if hist.status == DebtHistory.ACCEPTED:
                to_append_to_debt.append(hist)

            self.DB.add(hist)

        debts = DebtHistory.add_multi_to_debt(to_append_to_debt)

        [self.DB.add(debt) for debt in debts]


class SocialAuthMixin(ActivateUserMixin):
    def get_or_create(self, provider, remote_id, name, email):
        social_info = SocialInfo.get(
            provider=provider,
            remote_id=remote_id,
        )

        if social_info:
            refresh_token = Auth.get_or_create(social_info.user_id)
            return {
                'refresh_token': refresh_token.refresh_token,
            }

        # maybe user is already a member
        email_obj = self.DB.query(Email).filter_by(email=email).first()

        if email_obj:
            social_info = SocialInfo(
                provider=provider,
                remote_id=remote_id,
                user_id=email_obj.user_id,
            )
            self.DB.add(social_info)
            refresh_token = Auth.get_or_create(email_obj.user_id)
            return {
                'refresh_token': refresh_token.refresh_token,
            }

        user = User(
            name=name,
            password=uuid.uuid4().hex,
        )
        self.DB.add(user)
        self.DB.flush()

        social_info = SocialInfo(
            provider=provider,
            remote_id=remote_id,
            user_id=user.id,
        )
        self.DB.add(social_info)

        email_obj = Email(
            user_id=user.id,
            email=email,
            primary=True,
        )
        self.DB.add(email_obj)

        self.delete_all_emails_and_all_users(email_obj.email)
        self.append_debts_to_user(email_obj, user)

        refresh_token = Auth.get_or_create(user.id)
        return {
            'refresh_token': refresh_token.refresh_token,
        }


@view_config(route_name='facebook_auth', renderer='json')
class FacebookAuth(SocialAuthMixin, BaseAPI):

    @schema(schemas.auth.SocialLoginIn, schemas.auth.GetRTokenOut)
    def post(self, data):
        access_token = data['access_token']
        url = 'https://graph.facebook.com/me'
        url += '?access_token={access_token}'
        url = url.format(access_token=access_token)
        resp = requests.get(url)

        if resp.status_code != 200:
            raise dhexc.FacebookInvalidAccessToken(
                'Invalid Facebook access token'
            )

        data = resp.json()

        remote_id = data['id']
        name = data.get('name')
        email = data.get('email')
        if not email:
            raise dhexc.FacebookEmailNotProvided(
                'Email was not provided'
            )

        return self.get_or_create('facebook', remote_id, name, email)


@view_config(route_name='google_auth', renderer='json')
class GoogleAuth(SocialAuthMixin, BaseAPI):

    @schema(schemas.auth.SocialLoginIn, schemas.auth.GetRTokenOut)
    def post(self, data):
        url = 'https://www.googleapis.com/oauth2/v2/userinfo'
        url += '?access_token={access_token}'
        access_token = data['access_token']
        url = url.format(access_token=access_token)
        resp = requests.get(url)

        if resp.status_code != 200:
            raise dhexc.GoogleInvalidAccessToken(
                'Invalid Facebook access token'
            )
        data = resp.json()

        remote_id = data['id']
        name = data['name']
        email = data.get('email')
        verified_email = data.get('verified_email', False)

        if not verified_email:
            raise dhexc.GoogleEmailNotVerified(
                'Email is not verified'
            )

        if not email:
            raise dhexc.GoogleEmailNotProvided(
                'Email address was not provided'
            )

        return self.get_or_create('google', remote_id, name, email)


@view_config(route_name='email_auth', renderer='json')
class EmailAuth(BaseAPI):
    @schema(schemas.auth.GetRTokenIn, schemas.auth.GetRTokenOut)
    def get(self, data):
        """
        Return refresh token for given user.
        """
        email = data['email']
        password = data['password']

        try:
            email = self.DB.query(Email) \
                .filter(Email.primary==True) \
                .filter(Email.email==email).one()
        except NoResultFound:
            return HTTPNotFound()

        user = email.user
        if user.password != password:
            return HTTPNotFound()

        refresh_token = Auth.get_or_create(user.id)
        return {
            'refresh_token': refresh_token.refresh_token,
        }


@view_config(route_name='activate_user', renderer='json')
class ActivateUserAPI(ActivateUserMixin, BaseAPI):

    @schema(schemas.auth.ActivateUserIn)
    def get(self, data):
        activation = data['activation_code']
        query = self.DB.query(User, ActivationEmail) \
                       .filter(User.id==ActivationEmail.user_id) \
                       .filter(ActivationEmail.activation==activation)
        try:
            user, email = query.one()
        except NoResultFound:
            return HTTPNotFound()

        is_user_activated = any(user.emails)

        activated_email = Email(
            email=email.email,
            user_id=user.id,
            # if activated it has to have primary email
            primary=not is_user_activated,
        )

        self.DB.add(activated_email)

        self.delete_all_emails_and_all_users(email.email)
        self.append_debts_to_user(email, user)

        return HTTPOk()


@view_config(route_name='access_token', renderer='json')
class GetAccessToken(BaseAPI):
    @schema(schemas.auth.GetATokenIn, schemas.auth.GetATokenOut)
    def get(self, data):
        refresh_token = data['refresh_token']

        auth = Auth.get_by_refresh_token(refresh_token)
        if not auth:
            raise dhexc.InvalidRefreshToken('Invalid refresh_token')

        access_token = auth.generate_access_token()
        return {
            'access_token': access_token
        }


@view_config(route_name='login', renderer='json')
class Login(BaseAPI):

    @schema(schemas.auth.LoginUser)
    def post(self, data):
        """
        Use cookies to login user.
        This will be depreciated in the future.
        """
        email = data['email']
        password = data['password']

        try:
            user, email = self.DB.query(User, Email)\
                              .filter(User.id==Email.user_id) \
                              .filter(User.password==password)\
                              .filter(Email.email==email)\
                              .filter(Email.primary==True).one()
        except NoResultFound:
            return HTTPNotFound()
        headers = remember(self.request, user.id)
        self.request.response.headerlist.extend(headers)
        self.request.response.status = 200
        return {}
