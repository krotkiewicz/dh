from collections import defaultdict

from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden,\
    HTTPOk, HTTPNotFound
from pyramid.view import view_config

from dh.models.user import User
from dh.models.debt import DebtHistory, Debt
from dh.utils.views import BaseAPI
from dh.utils.schema import schema

from . import schemas

@view_config(route_name='debt_plus', renderer='json', permission='auth')
@view_config(route_name='debt_minus', renderer='json', permission='auth')
class DebtCollectionAPI(BaseAPI):

    @schema(schemas.debt.AddDebtIn, schemas.debt.AddDebtOut)
    def post(self, data):

        email = data['email']
        money = data['money']
        desc = data['desc']

        user, email_obj = User.get(email)

        email = email_obj and email_obj.email or email
        user_id = user and user.id or None

        if user_id == self.request.user.id:
            # cant add debt to yourself
            raise HTTPBadRequest()

        if 'plus' in self.request.path:
            debthistory = self.plus(user_id, email, money, desc)
        elif 'minus' in self.request.path:
            debthistory = self.minus(user_id, email, money, desc)
        else:
            raise HTTPNotFound

        is_debt_valid = (
            debthistory.status == DebtHistory.ACCEPTED and
            user_id is not None
        )

        if is_debt_valid:
            debt = debthistory.add_to_debt()
            self.DB.add(debt)

        self.request.response.status_code = 201
        return {
            'id': debthistory.id
        }


    def plus(self, user_id, email, money, desc):
        took_id = self.request.user.id
        debthistory = DebtHistory(
            gave_id=user_id,
            gave_email=email,
            took_id=took_id,
            desc=desc,
            money=money,
            status=DebtHistory.ACCEPTED,
        )

        self.DB.add(debthistory)
        self.DB.flush()
        return debthistory


    def minus(self, user_id, email, money, desc):
        gave_id = self.request.user.id
        debthistory = DebtHistory(
            gave_id=gave_id,
            took_id=user_id,
            took_email=email,
            desc=desc,
            money=money,
            status=DebtHistory.PENDING,
        )

        self.DB.add(debthistory)
        self.DB.flush()
        return debthistory


@view_config(route_name='debt_status', renderer='json', permission='auth')
class DebtStatusAPI(BaseAPI):

    @schema(out=schemas.debt.DebtStatusOut)
    def get(self, data):
        me_id = self.request.user.id
        debts = Debt.get(me_id)

        mine = [debt for debt in debts if debt.took_id == me_id]
        their = [debt for debt in debts if debt.took_id != me_id]

        debts_result = defaultdict(lambda: 0)

        for debt in their:
            debts_result[debt.tooks] += debt.money

        for debt in mine:
            debts_result[debt.gaves] -= debt.money

        result = [
            {'user': who.to_dict(), 'money': money}
            for who, money in debts_result.iteritems()
        ]
        return {
            'debts': result
        }


@view_config(route_name='debt_history', renderer='json', permission='auth')
class DebtHistoryAPI(BaseAPI):

    @schema(out=schemas.debt.DebtHistory)
    def get(self, data):
        me_id = self.request.user.id
        debts = DebtHistory.get(me_id)

        debts = [debt.to_dict() for debt in debts]
        return {
            'history': debts,
        }

@view_config(route_name='debt_accept', renderer='json', permission='auth')
class DebtAcceptAPI(BaseAPI):

    @schema(schemas.debt.DebtAcceptIn)
    def post(self, data):
        debt_id = data['id']

        debthist = DebtHistory.get(self.request.user.id, debt_id)

        if debthist.status != DebtHistory.PENDING:
            return HTTPForbidden()

        debthist.status = DebtHistory.ACCEPTED
        debt = debthist.add_to_debt()
        self.DB.add(debthist)
        self.DB.add(debt)

        return HTTPOk()
