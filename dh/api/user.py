from pyramid.httpexceptions import HTTPBadRequest, HTTPOk
from pyramid.view import view_config

from dh.utils.views import BaseAPI
from dh.utils.schema import schema
from . import schemas


@view_config(route_name='me', renderer='json', permission='auth')
class MeAPI(BaseAPI):
    @schema(schemas.user.MeIn, schemas.user.MeOut)
    def get(self, data):
        return {
            'name': self.request.user.name,
        }


@view_config(route_name='emails', renderer='json', permission='auth')
class EmailsAPI(BaseAPI):

    @schema(out=schemas.user.EmailsGetOut)
    def get(self, data):
        emails = self.request.user.emails.all()
        emails = [email.to_dict() for email in emails]

        not_verified = self.request.user.not_verified_emails.all()
        not_verified = [email.to_dict() for email in not_verified]
        emails.extend(not_verified)
        return {
            'emails': emails,
        }

    @schema(schemas.user.EmailsPostIn)
    def post(self, data):
        email = data['email']
        email = self.request.user.add_email(
            email=email,
        )
        self.DB.add(email)
        try:
            self.DB.flush()
        except Exception as e:
            return HTTPBadRequest()

        return HTTPOk()
