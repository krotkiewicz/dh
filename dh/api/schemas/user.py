import formencode
from formencode import validators


class MeIn(formencode.Schema):
    pass


class UserObj(formencode.Schema):
    id = validators.Int(not_empty=True)
    name = validators.UnicodeString(not_empty=True)


class MeOut(UserObj):
    pass


class EmailsGetIn(formencode.Schema):
    pass


class EmailObj(formencode.Schema):
    email = validators.Email(not_empty=True)
    primary = validators.Bool()
    verified = validators.Bool()


class EmailsGetOut(formencode.Schema):
    emails = formencode.ForEach(EmailObj)


class EmailsPostIn(formencode.Schema):
    email = validators.Email(not_empty=True)
