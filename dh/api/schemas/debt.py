import formencode
from formencode import validators

from .user import UserObj


class AddDebtIn(formencode.Schema):
    email = validators.Email(not_empty=True)
    money = validators.Int(not_empty=True)
    desc = validators.UnicodeString(not_empty=True)


class AddDebtOut(formencode.Schema):
    id = validators.Int(not_empty=True)


class DebtStatusObj(formencode.Schema):
    user = UserObj(not_empty=True)
    money = validators.Int(not_empty=True)


class DebtStatusOut(formencode.Schema):
    debts = formencode.ForEach(DebtStatusObj)


class DebtHistoryItem(formencode.Schema):
    id = validators.Int(not_empty=True)
    gave_id = validators.Int(not_empty=True)
    took_id = validators.Int(not_empty=True)
    tooks = UserObj(not_empty=True)
    gaves = UserObj(not_empty=True)
    gave_email = validators.Email(not_empty=True)
    took_email = validators.Email(not_empty=True)
    when = validators.DateValidator(not_empty=True)
    desc = validators.UnicodeString(not_empty=True)
    money = validators.Int(not_empty=True)
    status = validators.UnicodeString(not_empty=True)


class DebtHistory(formencode.Schema):
    history = formencode.ForEach(DebtHistoryItem)


class DebtAcceptIn(formencode.Schema):
    id = validators.Int(not_empty=True)
