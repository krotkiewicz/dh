import formencode
from formencode import validators


class CreateUserIn(formencode.Schema):
    name = validators.UnicodeString(not_empty=True)
    email = validators.Email(not_empty=True)
    password = validators.UnicodeString(min=6)


class ActivateUserIn(formencode.Schema):
    activation_code = validators.String(min=32, max=32)


class GetRTokenIn(formencode.Schema):
    email = validators.Email(not_empty=True)
    password = validators.String(min=6)


class GetRTokenOut(formencode.Schema):
    refresh_token = validators.String(not_empty=True)


class GetATokenIn(formencode.Schema):
    refresh_token = validators.String(not_empty=True)


class GetATokenOut(formencode.Schema):
    access_token = validators.String(not_empty=True)


class LoginUser(GetRTokenIn):
    pass


class SocialLoginIn(formencode.Schema):
    access_token = validators.String(not_empty=True)
