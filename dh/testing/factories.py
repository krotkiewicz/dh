from dh.models.user import User, Email, ActivationEmail, Auth

class UserMixin(object):
    def create_user(self, active=False, **kwargs):
        defaults = {
            'name': 'namex',
            'email': 'emailx@example.com',
            'password': 'passwordx'
        }
        defaults.update(kwargs)
        user, email = User.new(**defaults)
        if active:
            old_email = email
            email = Email(
                email=old_email.email,
                user_id=user.id,
                primary=True,
            )
            self.DB.expunge(old_email)
            self.DB.add(email)

        self.DB.flush()

        return user, email

    def get_access_token(self, user):
        refresh_token = Auth.get_or_create(user.id)
        access_token = refresh_token.generate_access_token()
        return access_token

    def get_notactivated_users(self, name=None):
        query = self.DB.query(User, ActivationEmail) \
            .filter(ActivationEmail.user_id==User.id)

        if name:
            query = query.filter(User.name==name)

        return query.all()
