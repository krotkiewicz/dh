import os
import logging
import unittest

from webtest import TestApp

from sqlalchemy import engine_from_config
from sqlalchemy.orm.session import Session
from pyramid import (
    paster,
    testing,
)

ROOT_PATH = os.path.dirname(__file__)
setting_file = os.path.join(ROOT_PATH, "../../etc", "test.ini")

# creating memcache before other modules are loaded :(
if os.path.isfile(setting_file):
    settings = paster.get_appsettings(setting_file)
else:
    settings = None

from dh.models import base
from dh.utils.request import Request
from dh.utils import b64

from .factories import UserMixin

connection = None
engine = None
app = None


def setup_module():
    global connection, app, engine
    if settings is None:
        raise Exception('Settings file not found %s' % setting_file)

    engine = engine_from_config(settings, prefix='sqlalchemy.')

    connection = engine.connect()
    base.Base.metadata.drop_all(engine)

    base.DBSession.configure(bind=engine)
    base.Base.metadata.create_all(engine)

    app = TestApp(paster.get_app(setting_file))

    logger = logging.getLogger('txn')
    logger.propagate = False
    logger.setLevel(logging.WARNING)


def teardown_module():
    global connection, engine

    base.Base.metadata.drop_all(engine)
    connection.close()


class BaseTestCase(UserMixin, unittest.TestCase):

    def setUp(self):
        self._transaction = connection.begin()
        base.DBSession_ = Session(connection)
        self.DB = base.DBSession

    def tearDown(self):
        base.DBSession_.close()
        self._transaction.rollback()

    def assertLenEq(self, iterable, length):
        assert len(iterable) == length, 'Length of %s != %s' % (iterable, length)

class TestCase(BaseTestCase):

    def setUp(self):
        super(TestCase, self).setUp()
        self.config = testing.setUp()

        self.request = Request()

    def tearDown(self):
        super(TestCase, self).tearDown()
        testing.tearDown()


class WebTestCase(BaseTestCase):

    def tearDown(self):
        super(WebTestCase, self).tearDown()
        app.reset()

    def add_auth(self, kwargs):
        if 'token' in kwargs:
            headers = kwargs.setdefault('headers', {})
            headers['X-AuthToken'] = kwargs.pop('token')

    def get(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.get(*args, **kwargs)

    def post(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.post(*args, **kwargs)

    def post_json(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.post_json(*args, **kwargs)

    def put(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.put(*args, **kwargs)

    def put_json(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.put_json(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.delete(*args, **kwargs)

    def delete_json(self, *args, **kwargs):
        self.add_auth(kwargs)
        return app.delete_json(*args, **kwargs)

    def login(self, name, email):
        pass
