from pyramid.httpexceptions import HTTPNotFound, HTTPBadRequest

class APIError(Exception):
    dhcode = None
    """Generic Error"""


class InvalidRefreshToken(APIError, HTTPNotFound):
    dhcode = 2001


class FacebookInvalidAccessToken(APIError, HTTPBadRequest):
    dhcode = 2011


class FacebookEmailNotProvided(APIError, HTTPBadRequest):
    dbcode = 2012


class GoogleInvalidAccessToken(APIError, HTTPBadRequest):
    dhcode = 2021


class GoogleEmailNotProvided(APIError, HTTPBadRequest):
    dhcode = 2022


class GoogleEmailNotVerified(APIError, HTTPBadRequest):
    dhcode = 2023
