import unittest

from dh.testing import WebTestCase

from dh.models.user import User, Email, ActivationEmail
from dh.models.debt import DebtHistory


class TestDebtAdd(WebTestCase):

    def test_plus_debt(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        you, you_email = self.create_user(
            name='b',
            email='b@o2.pl',
            active=True,
        )
        MONEY = 123
        DESC = 'dupa'

        data = {
            'email': you_email.email,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/plus', data, token=me_access_token)
        history = self.DB.query(DebtHistory).one()

        self.assertEqual(history.money, MONEY)
        self.assertEqual(history.desc, DESC)

        self.assertEqual(history.gave_id, you.id)
        self.assertEqual(history.took_id, me.id)
        self.assertEqual(history.gave_email, you_email.email)
        self.assertEqual(history.took_email, None)

    def test_minus_debt(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        you, you_email = self.create_user(
            name='b',
            email='b@o2.pl',
            active=True,
        )
        MONEY = 123
        DESC = 'dupa'

        data = {
            'email': you_email.email,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/minus', data, token=me_access_token)
        history = self.DB.query(DebtHistory).one()

        self.assertEqual(history.money, MONEY)
        self.assertEqual(history.desc, DESC)

        self.assertEqual(history.gave_id, me.id)
        self.assertEqual(history.took_id, you.id)
        self.assertEqual(history.gave_email, None)
        self.assertEqual(history.took_email, you_email.email)


    def test_plus_debt_no_user(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        YOU_EMAIL = 'b@o2.pl'
        MONEY = 123
        DESC = 'dupa'

        data = {
            'email': YOU_EMAIL,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/plus', data, token=me_access_token)
        history = self.DB.query(DebtHistory).one()

        self.assertEqual(history.money, MONEY)
        self.assertEqual(history.desc, DESC)

        self.assertEqual(history.gave_id, None)
        self.assertEqual(history.took_id, me.id)
        self.assertEqual(history.gave_email, YOU_EMAIL)
        self.assertEqual(history.took_email, None)


    def test_minus_debt_no_user(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        YOU_EMAIL = 'b@o2.pl'
        MONEY = 123
        DESC = 'dupa'

        data = {
            'email': YOU_EMAIL,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/minus', data, token=me_access_token)
        history = self.DB.query(DebtHistory).one()

        self.assertEqual(history.money, MONEY)
        self.assertEqual(history.desc, DESC)

        self.assertEqual(history.gave_id, me.id)
        self.assertEqual(history.took_id, None)
        self.assertEqual(history.gave_email, None)
        self.assertEqual(history.took_email, YOU_EMAIL)



class TestDebtSum(WebTestCase):

    def test_debt_my_sum(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        you, you_email = self.create_user(
            name='b',
            email='b@o2.pl',
            active=True,
        )
        MONEY = 123
        DESC = 'dupa'

        data = {
            'email': you_email.email,
            'money': MONEY,
            'desc': DESC,
        }

        self.post_json('/v1/debt/plus', data, token=me_access_token)
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -123)

        self.post_json('/v1/debt/plus', data, token=me_access_token)
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -246)

        data['money'] = 100
        resp = self.post_json('/v1/debt/minus', data, token=me_access_token)
        self.post_json(
            '/v1/debt/%s/accept' % resp.json['id'],
            token=me_access_token,
        )
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -146)

        data['money'] = 146
        resp = self.post_json('/v1/debt/minus', data, token=me_access_token)
        self.post_json(
            '/v1/debt/%s/accept' % resp.json['id'],
            token=me_access_token,
        )
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], 0)

    def test_debt_sum_me_and_you(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        you, you_email = self.create_user(
            name='b',
            email='b@o2.pl',
            active=True,
        )
        you_access_token = self.get_access_token(you)
        MONEY = 13
        DESC = 'dupa'

        data = {
            'email': you_email.email,
            'money': MONEY,
            'desc': DESC,
        }

        self.post_json('/v1/debt/plus', data, token=me_access_token)
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -13)

        data['email'] = me_email.email
        data['money'] = 11
        self.post_json('/v1/debt/plus', data, token=you_access_token)
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -2)

        data['email'] = you_email.email
        data['money'] = 17
        resp = self.post_json('/v1/debt/minus', data, token=me_access_token)
        self.post_json(
            '/v1/debt/%s/accept' % resp.json['id'],
            token=me_access_token,
        )
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], 15)

        data['email'] = me_email.email
        data['money'] = 23
        resp = self.post_json('/v1/debt/minus', data, token=you_access_token)
        self.post_json(
            '/v1/debt/%s/accept' % resp.json['id'],
            token=me_access_token,
        )
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -8)


class TestCantAddDebtToYourself(WebTestCase):

    def test_plus(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        MONEY = 13
        DESC = 'dupa'

        data = {
            'email': me_email.email,
            'money': MONEY,
            'desc': DESC,
        }

        self.post_json(
            '/v1/debt/plus',
            data,
            token=me_access_token,
            status=400,
        )
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 0)

    def test_minus(self):
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        MONEY = 13
        DESC = 'dupa'

        data = {
            'email': me_email.email,
            'money': MONEY,
            'desc': DESC,
        }

        self.post_json(
            '/v1/debt/minus',
            data,
            token=me_access_token,
            status=400,
        )
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 0)


class CreateUserWithDebt(WebTestCase):

    def test_plus_debt_user_created_after(self):
        """
        When we add debt to non existing user, it should be there when he registers.
        """
        me, me_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        YOUR_EMAIL = 'b@o2.pl'
        YOUR_NAME = 'b'
        MONEY = 123
        DESC = 'dupa'

        data = {
            'email': YOUR_EMAIL,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/plus', data, token=me_access_token)
        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 0)

        resp = self.post_json('/v1/users', {
            'name': YOUR_NAME,
            'email': YOUR_EMAIL,
            'password': 'passwordx'

        })

        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 0)

        you, your_email = self.get_notactivated_users(YOUR_NAME)[0]

        self.get('/v1/activate/%s' % your_email.activation)

        resp = self.get('/v1/debt/status', token=me_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], -123)

        you, your_email = User.get(YOUR_EMAIL)
        you_access_token = self.get_access_token(you)

        resp = self.get('/v1/debt/status', token=you_access_token)
        debts = resp.json['debts']
        self.assertLenEq(debts, 1)
        self.assertEqual(debts[0]['money'], 123)

    def test_plus_debt_user_add_email_after(self):
        """
        When an user add new email, it should obtain all debts
        assigned to it.
        """
        me, my_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        you, your = self.create_user(
            name='b',
            email='b@o2.pl',
            active=True,
        )
        you_access_token = self.get_access_token(you)
        NEXT_EMAIL = 'b2@o2.pl'
        MONEY = 123
        DESC = 'dupa'

        self.post_json(
            '/v1/me/emails',
            {'email': NEXT_EMAIL},
            token=you_access_token,
        )

        data = {
            'email': NEXT_EMAIL,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/plus', data, token=me_access_token)

        resp = self.get('/v1/debt/status', token=me_access_token)
        self.assertListEqual(resp.json['debts'], [])
        resp = self.get('/v1/debt/status', token=you_access_token)
        self.assertListEqual(resp.json['debts'], [])

        next_email = self.DB.query(ActivationEmail).one()

        self.get('/v1/activate/%s' % next_email.activation)

        resp = self.get('/v1/debt/status', token=me_access_token)
        self.assertDictContainsSubset(
            resp.json['debts'][0],
            {'money': -123, 'user': {'id': 2, 'name': 'b'}}
        )
        resp = self.get('/v1/debt/status', token=you_access_token)
        self.assertDictContainsSubset(
            resp.json['debts'][0],
            {'money': 123, 'user': {'id': 1, 'name': 'a'}}
        )

    def test_user_add_debt_to_his_own_email(self):
        """
        Check if user can add debt to email and then register it to his profile
        """
        me, my_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)
        NEXT_EMAIL = 'b2@o2.pl'
        MONEY = 123
        DESC = 'dupa'

        self.post_json(
            '/v1/me/emails',
            {'email': NEXT_EMAIL},
            token=me_access_token,
        )

        data = {
            'email': NEXT_EMAIL,
            'money': MONEY,
            'desc': DESC,
        }

        resp = self.post_json('/v1/debt/plus', data, token=me_access_token)

        next_email = self.DB.query(ActivationEmail).one()

        self.get('/v1/activate/%s' % next_email.activation)

        resp = self.get('/v1/debt/status', token=me_access_token)
        self.assertListEqual(resp.json['debts'], [])


class DebtHistoryTest(WebTestCase):
    def test_debt_history(self):
        MONEY = 10

        me, my_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)

        YOUR_NAME = 'b'
        YOUR_EMAIL = 'b@o2.pl'
        you, your_email = self.create_user(
            name=YOUR_NAME,
            email=YOUR_EMAIL,
            active=True,
        )

        data = {
            'email': YOUR_EMAIL,
            'money': MONEY,
            'desc': 'aaa',
        }

        self.post_json('/v1/debt/plus', data, token=me_access_token)
        self.post_json('/v1/debt/plus', data, token=me_access_token)
        self.post_json('/v1/debt/plus', data, token=me_access_token)

        resp = self.get('/v1/debt', token=me_access_token)
        self.assertLenEq(resp.json['history'], 3)

    def test_accept_debt(self):
        MONEY = 10

        me, my_email = self.create_user(
            name='a',
            email='a@o2.pl',
            active=True,
        )
        me_access_token = self.get_access_token(me)

        YOUR_NAME = 'b'
        YOUR_EMAIL = 'b@o2.pl'
        you, your_email = self.create_user(
            name=YOUR_NAME,
            email=YOUR_EMAIL,
            active=True,
        )
        you_access_token = self.get_access_token(you)

        data = {
            'email': YOUR_EMAIL,
            'money': MONEY,
            'desc': 'aaa',
        }

        resp = self.post_json('/v1/debt/minus', data, token=me_access_token)
        debt_hist_id = resp.json['id']

        resp = self.get('/v1/debt/status', token=you_access_token)

        self.assertDictEqual(resp.json, {'debts': []})

        self.post('/v1/debt/%s/accept' % debt_hist_id, token=me_access_token)

        resp = self.get('/v1/debt/status', token=me_access_token)

        self.assertDictContainsSubset(
            resp.json['debts'][0],
            {'money': 10, 'user': {'id': 2, 'name': 'b'}}
        )
