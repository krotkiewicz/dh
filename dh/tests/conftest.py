import pytest
import dh.testing as testing

setup_module = testing.setup_module
teardown_ = testing.teardown_module

@pytest.fixture(scope="session", autouse=True)
def setup_testbed(request):
    testing.setup_module()
    request.addfinalizer(testing.teardown_module)
