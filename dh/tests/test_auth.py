import mock

from dh.testing import WebTestCase

from dh.models.user import User, ActivationEmail, Email, SocialInfo


class TestAuth(WebTestCase):

    def test_create_user(self):
        resp = self.post_json('/v1/users', {
            'name': 'namex',
            'email': 'emailx@example.com',
            'password': 'passwordx'

        })
        self.assertEqual(resp.status_code, 201)

        user = self.DB.query(User).one()
        self.assertEqual(user.name, 'namex')
        self.assertEqual(user.password, 'passwordx')

        email = user.not_verified_emails.one()

        self.assertEqual(email.email, 'emailx@example.com')
        self.assertIsNotNone(email.activation)
        self.assertGreater(len(email.activation), 10)

    def test_activate_user(self):
        user, email = self.create_user()
        self.assertIsNotNone(email.activation)
        self.assertGreater(len(email.activation), 10)
        self.get('/v1/activate/%s' % email.activation)

        emails = self.DB.query(ActivationEmail).all()
        self.assertLenEq(emails, 0)

        self.get('/v1/activate/%s' % email.activation, status=404)

    def test_activate_user_wrong_code(self):
        user, email = self.create_user()
        wrong_activation = list(email.activation)
        wrong_activation[0] = 'z'
        self.get(
            '/v1/activate/%s' % ''.join(wrong_activation),
            status=404,
        )

    def test_get_token(self):
        user, email = self.create_user(active=True)

        resp = self.get(
            '/v1/auth/email',
            params={
                'email': 'emailx@example.com',
                'password': 'passwordx'
            },
        )
        refresh_token = resp.json['refresh_token']
        resp = self.get(
            '/v1/auth/access_token',
            params={
                'refresh_token': refresh_token
            },
        )
        access_token = str(resp.json['access_token'])
        self.get('/v1/me', token=access_token, status=200)
        # lets mess with token a little bit:
        access_token = access_token[5:] + access_token[:5]
        self.get('/v1/me', token=access_token, status=403)

    @mock.patch('dh.api.auth.requests')
    def test_facebook_auth(self, requestsm):
        facebook_response = {
            'email': 'konrad.xxx@gmail.com',
            'first_name': 'Konrad',
            'gender': 'male',
            'id': '296517263862794',
            'last_name': 'Xxx',
            'link': 'https://www.facebook.com/profile.php?id=100025282047155',
            'locale': 'en_US',
            'name': 'Konrad Xxx',
            'timezone': 2,
            'updated_time': '2014-09-23T06:48:01+0000',
            'verified': False,
        }
        requestsm.get().status_code = 200
        requestsm.get().json.return_value = facebook_response

        resp1 = self.post_json(
            '/v1/auth/facebook',
            {
                'access_token': 'FACEBOOK_ACCESS_TOKEN',
            },
        )
        resp2 = self.post_json(
            '/v1/auth/facebook',
            {
                'access_token': 'FACEBOOK_ACCESS_TOKEN',
            },
        )
        assert resp1.json['refresh_token'] == resp2.json['refresh_token']

        user = self.DB.query(User).one()
        email = self.DB.query(Email).one()
        social_info = self.DB.query(SocialInfo).one()

        assert user.name == 'Konrad Xxx'
        assert self.DB.query(ActivationEmail).all() == []
        assert email == user.emails.one()
        assert email.email == 'konrad.xxx@gmail.com'
        assert social_info.provider.code == 'facebook'
        assert social_info.remote_id == '296517263862794'
        assert social_info.user_id == user.id

    @mock.patch('dh.api.auth.requests')
    def test_google_auth(self, requestsm):
        google_response = {
            'email': 'konrad.xxx@gmail.com',
            'family_name': 'xxx',
            'gender': 'male',
            'given_name': 'Konrad',
            'hd': 'scrapinghub.com',
            'id': '107679281152130479742',
            'link': 'https://plus.google.com/1',
            'name': 'Konrad Xxx',
            'picture': 'https://lh4.googleusercontent.com/Ss/photo.jpg',
            'verified_email': True
        }
        requestsm.get().status_code = 200
        requestsm.get().json.return_value = google_response

        resp1 = self.post_json(
            '/v1/auth/google',
            {
                'access_token': 'GOOGLE_ACCESS_TOKEN',
            },
        )
        resp2 = self.post_json(
            '/v1/auth/google',
            {
                'access_token': 'GOOGLE_ACCESS_TOKEN',
            },
        )
        assert resp1.json['refresh_token'] == resp2.json['refresh_token']

        user = self.DB.query(User).one()
        email = self.DB.query(Email).one()
        social_info = self.DB.query(SocialInfo).one()

        assert user.name == 'Konrad Xxx'
        assert self.DB.query(ActivationEmail).all() == []
        assert email == user.emails.one()
        assert email.email == 'konrad.xxx@gmail.com'
        assert social_info.provider.code == 'google'
        assert social_info.remote_id == '107679281152130479742'
        assert social_info.user_id == user.id

    @mock.patch('dh.api.auth.requests')
    def test_google_and_then_facebook_auth(self, requestsm):
        # lets check scenario with the same email for two social providers

        google_response = {
            'email': 'konrad.xxx@gmail.com',
            'family_name': 'xxx',
            'gender': 'male',
            'given_name': 'Konrad',
            'hd': 'scrapinghub.com',
            'id': '107679281152130479742',
            'link': 'https://plus.google.com/1',
            'name': 'Konrad Xxx',
            'picture': 'https://lh4.googleusercontent.com/-Ss/photo.jpg',
            'verified_email': True
        }
        facebook_response = {
            'email': 'konrad.xxx@gmail.com',
            'first_name': 'Konrad',
            'gender': 'male',
            'id': '296517263862794',
            'last_name': 'Xxx',
            'link': 'https://www.facebook.com/profile.php?id=100025282047155',
            'locale': 'en_US',
            'name': 'Konrad Xxx',
            'timezone': 2,
            'updated_time': '2014-09-23T06:48:01+0000',
            'verified': False,
        }
        requestsm.get().status_code = 200
        requestsm.get().json.return_value = google_response

        resp1 = self.post_json(
            '/v1/auth/google',
            {
                'access_token': 'GOOGLE_ACCESS_TOKEN',
            },
        )

        requestsm.get().json.return_value = facebook_response
        resp2 = self.post_json(
            '/v1/auth/facebook',
            {
                'access_token': 'FACEBOOK_ACCESS_TOKEN',
            },
        )
        assert resp1.json['refresh_token'] == resp2.json['refresh_token']

        user = self.DB.query(User).one()
        email = self.DB.query(Email).one()
        fb_si = self.DB.query(SocialInfo).filter_by(provider='facebook').one()
        goo_si = self.DB.query(SocialInfo).filter_by(provider='google').one()

        assert user.name == 'Konrad Xxx'
        assert self.DB.query(ActivationEmail).all() == []
        assert email == user.emails.one()
        assert email.email == 'konrad.xxx@gmail.com'
        assert fb_si.user_id == goo_si.user_id == user.id
        assert fb_si.remote_id == '296517263862794'
        assert goo_si.remote_id == '107679281152130479742'


class WeirdCasesAuth(WebTestCase):

    def test_registers_the_same_email(self):
        EMAIL = 'emailx@example.com'
        NAME1 = 'namex1'
        NAME2 = 'namex2'
        resp = self.post_json('/v1/users', {
            'name': NAME1,
            'email': EMAIL,
            'password': 'passwordx'

        })

        resp = self.post_json('/v1/users', {
            'name': NAME2,
            'email': EMAIL,
            'password': 'passwordx'

        })

        user, email = self.DB.query(User, ActivationEmail) \
                          .filter(ActivationEmail.user_id==User.id)\
                          .filter(User.name==NAME1).one()

        user, name = self.get_notactivated_users(NAME1)[0]


        self.get('/v1/activate/%s' % email.activation)

        result = self.get_notactivated_users(NAME2)

        self.assertLenEq(result, 0)
        users_count = self.DB.query(User).count()
        self.assertEqual(users_count, 1)


    def test_user_adds_email_sombody_registers_with_it(self):
        #first user with email
        user, email = self.create_user(active=True, name='first')
        access_token = self.get_access_token(user)
        NEXT_EMAIL = 'next_email@example.com'

        data = {
            'email': NEXT_EMAIL,
        }

        # lets add NEXT_EMAIL to first user
        self.post_json(
            '/v1/me/emails',
            data,
            token=access_token,
        )

        # first user, NEXT_EMAIL
        fuser, femail = self.get_notactivated_users('first')[0]

        resp = self.post_json('/v1/users', {
            'name': 'second',
            'email': NEXT_EMAIL,
            'password': 'passwordx'

        })

        # second user, NEXT_EMAIL
        suser, semail = self.get_notactivated_users('second')[0]

        # lets activate second user !
        resp = self.get('/v1/activate/%s' % semail.activation)
        result = self.get_notactivated_users()
        self.assertLenEq(result, 0)

        # activation NEXT_EMAIL of first user shouldn't work:
        resp = self.get('/v1/activate/%s' % femail.activation, status=404)

        faccess_token = self.get_access_token(fuser)
        fuser_emails = self.get('/v1/me/emails', token=faccess_token)
        self.assertDictEqual(
            {
                'emails': [
                    {
                        'email': 'emailx@example.com',
                        'primary': True,
                        'verified': True,
                    },
                ],
            },
            fuser_emails.json,
        )
        saccess_token = self.get_access_token(suser)
        suser_emails = self.get('/v1/me/emails', token=saccess_token)
        self.assertDictEqual(
            {
                'emails': [
                    {
                        'email': 'next_email@example.com',
                        'primary': True,
                        'verified': True,
                    },
                ],
            },
            suser_emails.json,
        )


    def test_user_register_with_email_but_sombody_add_it(self):
        user, email = self.create_user(active=True, name='first')
        access_token = self.get_access_token(user)

        NEXT_EMAIL = 'next_email@example.com'

        self.post_json('/v1/users', {
            'name': 'second',
            'email': NEXT_EMAIL,
            'password': 'passwordx'

        })

        self.post_json(
            '/v1/me/emails',
            {'email': NEXT_EMAIL},
            token=access_token,
        )

        fuser, femail = self.get_notactivated_users('first')[0]

        suser, semail = self.get_notactivated_users('second')[0]

        self.get('/v1/activate/%s' % femail.activation)
        self.get('/v1/activate/%s' % semail.activation, status=404)

        users_count = self.DB.query(User).count()
        self.assertEqual(users_count, 1)
