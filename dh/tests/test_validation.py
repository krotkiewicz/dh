from dh.testing import WebTestCase

from dh.models.user import User, ActivationEmail


class TestInvalid(WebTestCase):
    def test_invalid(self):
        resp = self.post_json('/v1/users', {
            'email': 'emailxexample.com',
            'password': 'passwordx'

        }, status=400)

        data = resp.json

        expected = {
            'code': 1001,
            'data': {
                'email': 'An email address must contain a single @',
                'name': 'Missing value',
            },
            'message': 'Validation Error',
        }
        self.assertDictEqual(data, expected)

    def test_invalid_refresh_token(self):
        resp = self.get(
            '/v1/auth/access_token',
            params={
                'refresh_token': 'a',
            },
            status=404,
        )
        expected = {
            'code': 2001,
            'data': {
                'generic': 'Invalid refresh_token',
            },
            'message': 'The resource could not be found.'
        }
        self.assertDictEqual(
            resp.json,
            expected,
        )
