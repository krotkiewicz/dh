from dh.testing import WebTestCase

from dh.models.user import User, Email, ActivationEmail


class TestUser(WebTestCase):
    def test_me(self):
        user, email = self.create_user(active=True)
        access_token = self.get_access_token(user)
        self.get('/v1/me', token=access_token, status=200)

    def test_add_email(self):
        user, email = self.create_user(active=True)
        access_token = self.get_access_token(user)

        data = {
            'email': 'emailx2@example.com',
        }

        resp = self.post_json(
            '/v1/me/emails',
            data,
            token=access_token,
        )

        email = self.DB.query(ActivationEmail).one()

        self.assertEqual(email.email, 'emailx2@example.com')
        self.assertIsNotNone(email.activation)
        self.assertGreater(len(email.activation), 10)

        self.get('/v1/activate/%s' % email.activation)

        emails = self.DB.query(Email).filter(Email.user_id==user.id).all()

        emails = {
            email.email: email
            for email in emails
        }

        email = emails['emailx2@example.com']

        self.assertFalse(email.primary)

    def test_get_emails(self):
        user, email = self.create_user(active=True)
        access_token = self.get_access_token(user)
        email1 = user.add_email('emailx1@example.com')
        self.DB.add(email1)
        email2 = user.add_email('emailx2@example.com')
        self.DB.add(email2)

        self.DB.flush()

        resp = self.get('/v1/me/emails', token=access_token)

        expected = {
            'emails': [
                {
                    'email': 'emailx@example.com',
                    'primary': True,
                    'verified': True,
                },
                {
                    'email': 'emailx1@example.com',
                    'primary': False,
                    'verified': False,
                },
                {
                    'email': 'emailx2@example.com',
                    'primary': False,
                    'verified': False,
                },
            ],
        }

        self.assertDictEqual(resp.json, expected)
