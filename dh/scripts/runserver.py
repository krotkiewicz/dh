import logging
import os
import sys

from paste.deploy import loadapp
from paste.script.cherrypy_server import cpwsgi_server


def main(argv=sys.argv):
    try:
        import newrelic.agent
        newrelic_path = '%s/etc/newrelic.ini' % os.getcwd()
        newrelic.agent.initialize(newrelic_path)
    except Exception as e:
        logging.exception('New relic not initialized')


    if len(argv) > 1:
        config_path = argv[1]
    else:
        config_path = 'etc/config.ini'
        print 'no config provided using %s' % config_path

    port = int(os.environ.get("PORT", 5000))
    wsgi_app = loadapp('config:%s' % config_path, relative_to='.')
    cpwsgi_server(
        wsgi_app, host='0.0.0.0', port=port,
        numthreads=10,
        request_queue_size=200,
    )
