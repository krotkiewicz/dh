from __future__ import absolute_import
import sys
import os

from celery.bin import worker
from pyramid.paster import bootstrap


def main(argv=sys.argv):
    if len(argv) > 1:
        config_path = argv[1]
    else:
        config_path = 'etc/config.ini'
        print 'no config provided using %s' % config_path
        config_path = os.path.realpath(config_path)

    env = bootstrap(config_path)
    # after bootstraping our webapp, celery app is ready in dh.utils.celery:
    # also thanks to venusian library that is used by pyramid `config.scan()`
    # we get all tasks functions detection for free.
    from dh.utils.celery import celery as celery_app

    worker.worker(app=celery_app).run(loglevel='INFO')

