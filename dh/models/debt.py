import datetime

from sqlalchemy import Column, ForeignKey, orm, or_
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.types import String, Boolean, Integer, DateTime, Text

from .base import Base, DBSession
from .user import User

class DebtHistory(Base):
    __tablename__ = 'debt_history'
    EXCLUDE = ('gave_id', 'took_id')
    INCLUDE = ('tooks', 'gaves')

    PENDING = 'PENDING'
    ACCEPTED = 'ACCEPTED'
    REJECTED = 'REJECTED'

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    gave_id = Column(Integer, ForeignKey(User.id), nullable=True, index=True)
    took_id = Column(Integer, ForeignKey(User.id), nullable=True, index=True)
    gave_email = Column(String, nullable=True, index=True)
    took_email = Column(String, nullable=True, index=True)
    when = Column(DateTime, nullable=False, index=True, default=datetime.datetime.now)
    desc = Column(Text, nullable=False)
    money = Column(Integer, nullable=False)
    status = Column(String, default=PENDING)

    tooks = orm.relationship('User', foreign_keys=[took_id])
    gaves = orm.relationship('User', foreign_keys=[gave_id])

    @classmethod
    def my(cls, user_id):
        return or_(cls.gave_id==user_id, cls.took_id==user_id)

    @classmethod
    def get(cls, user_id, debt_id=None):
        query = DBSession.query(cls).filter(cls.my(user_id))

        if debt_id is not None:
            query = query.filter(cls.id==debt_id)
            result = query.one()
        else:
            result = query.all()

        return result

    def get_our(self, user_id1, user_id2):
        pass

    def add_to_debt(self, debt=None):
        if debt is None:
            try:
                debt = DBSession.query(Debt) \
                                .filter(Debt.gave_id==self.gave_id) \
                                .filter(Debt.took_id==self.took_id).one()
            except NoResultFound:
                debt = Debt(
                    gave_id=self.gave_id,
                    took_id=self.took_id,
                    money=0,
                )

        debt.money += self.money
        return debt

    @classmethod
    def add_multi_to_debt(cls, debthistories):
        cache = {}

        for hist in debthistories:
            debt = cache.get((hist.gave_id, hist.took_id))
            debt = hist.add_to_debt(debt=debt)
            cache[debt.gave_id, debt.took_id] = debt

        return cache.values()


class Debt(Base):
    __tablename__ = 'debts'
    EXCLUDE = ('id', 'gave_id', 'took_id')

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    gave_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    took_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    money = Column(Integer, nullable=False)

    tooks = orm.relationship('User', foreign_keys=[took_id])
    gaves = orm.relationship('User', foreign_keys=[gave_id])

    @classmethod
    def my(cls, user_id):
        return or_(cls.gave_id==user_id, cls.took_id==user_id)

    @classmethod
    def get(cls, user_id):
        debts = DBSession.query(cls).filter(cls.my(user_id)).all()
        return debts




