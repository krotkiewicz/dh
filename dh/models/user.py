import datetime
import hashlib
import hmac
import time
import uuid

from sqlalchemy import Column, ForeignKey, orm
from sqlalchemy.types import String, Boolean, Integer, DateTime
from sqlalchemy_utils import ChoiceType
from sqlalchemy.orm.exc import NoResultFound

from dh import settings
from dh.utils import b64

from .base import Base, DBSession



class InvalidAccessTokenError(Exception):
    """Raised by unpack_access_token"""

class AccessTokenExpiredError(Exception):
    """Raised by unpack_access_token"""


class Token(object):
    BASE = 'dh_%s'

    @classmethod
    def dump(cls, user_id, secret):
        token = '%s:%s' % (user_id, secret)
        token = b64.encode(token)
        token = cls.BASE % token
        return token

    @classmethod
    def loads(cls, token):
        prefix, data = token.split('_')
        user_id, secret = b64.decode(data).split(':')
        return user_id, secret


class User(Base):
    __tablename__ = 'users'
    EXCLUDE = ('password',)

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    name = Column(String, nullable=False)
    password = Column(String, nullable=False)

    emails = orm.relationship('Email', backref='user', lazy='dynamic')
    not_verified_emails = orm.relationship('ActivationEmail', backref='user', lazy='dynamic')

    @classmethod
    def new(cls, *args, **kwargs):
        email = kwargs.pop('email')
        user = cls(*args, **kwargs)
        DBSession.add(user)
        DBSession.flush()
        email = user.add_email(email)
        DBSession.add(email)
        return user, email

    def add_email(self, email):
        email = ActivationEmail(
            email=email,
            user_id=self.id,
            activation=uuid.uuid4().hex,
        )
        return email

    @classmethod
    def get(cls, email):
        try:
            email = DBSession.query(Email).filter(Email.email==email).one()
        except NoResultFound:
            return None, None

        user = email.user
        return user, email


class Auth(Base):
    __tablename__ = 'auth'

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    user_id = Column(
        Integer, ForeignKey(User.id),
        nullable=False, index=True, unique=True
    )
    refresh_token = Column(String, nullable=False)

    created_at = Column(DateTime, default=datetime.datetime.now)
    last_modified = Column(DateTime, onupdate=datetime.datetime.now)
    expires_at = Column(DateTime, nullable=True)

    @classmethod
    def get_by_refresh_token(cls, refresh_token):
        return DBSession.query(cls).filter(cls.refresh_token==refresh_token).first()

    @classmethod
    def get_or_create(cls, user_id):
        try:
            auth = DBSession.query(cls).filter(cls.user_id==user_id).one()
        except NoResultFound:
            auth = cls(user_id=user_id, refresh_token=uuid.uuid4().hex)
            DBSession.add(auth)

        return auth

    def generate_access_token(self):
        expires_sec = int(settings['AUTH_ACCESS_TOKEN_EXPIRES'])
        now = datetime.datetime.now()
        expires = now + datetime.timedelta(seconds=expires_sec)
        return self._make_access_token(self.user_id, [], expires)

    @classmethod
    def get_user_from_access_token(cls, access_token):
        try:
            auth_string = access_token.decode('base64')
            visible_data, hashed_data = auth_string.split('|')
            our_hash = cls._hash_with_secret(visible_data)
            if hashed_data != our_hash:
                raise InvalidAccessTokenError
            user_id, scopes, expires = visible_data.split(':')
            scopes = scopes.split(',')
            expires = datetime.datetime.fromtimestamp(float(expires))
            user_id = int(user_id)
        except:
            raise InvalidAccessTokenError

        if expires < datetime.datetime.now():
            raise AccessTokenExpiredError

        return user_id, scopes, expires

    @classmethod
    def _hash_with_secret(cls, data):
        ahash =  hmac.new(
            settings['AUTH_SECRET_KEY'],
            data,
            hashlib.sha256
        )
        ahash = ahash.hexdigest()
        return ahash

    @classmethod
    def _make_access_token(cls, uid, scopes, expires):
        """Makes auth string for given id, scopes, issue datetime"""
        expires_timestamp = str(time.mktime(expires.timetuple()))
        data = '{uid}:{scopes}:{expires_timestamp}'.format(
            uid=uid,
            scopes=','.join(scopes),
            expires_timestamp=expires_timestamp,
        )
        hashed_data = cls._hash_with_secret(data)
        full_data = '{visible_data}|{hashed_data}'.format(
            visible_data=data,
            hashed_data=hashed_data,
        )
        result = full_data.encode('base64').replace('\n', '')
        return result


class Email(Base):
    __tablename__ = 'emails'
    EXCLUDE = ('activation', 'user_id')

    email = Column(String, primary_key=True, nullable=False, index=True)
    user_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    primary = Column(Boolean, nullable=False, index=True)

    def to_dict(self):
        result = super(Email, self).to_dict()
        result['verified'] = True
        return result


class ActivationEmail(Base):
    __tablename__ = 'activation_emails'
    EXCLUDE = ('id', 'activation', 'user_id')

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    email = Column(String, nullable=False, index=True)
    user_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    activation = Column(String, nullable=True)

    def to_dict(self):
        result = super(ActivationEmail, self).to_dict()
        result['primary'] = False
        result['verified'] = False
        return result


class SocialInfo(Base):
    __tablename__ = 'social_info'
    PROVIDERS = (
        ('facebook', 'Facebook'),
        ('google', 'Google'),
    )
    id = Column(Integer, primary_key=True, nullable=False, index=True)
    provider = Column(ChoiceType(PROVIDERS), nullable=False)
    user_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    remote_id = Column(String, nullable=False, index=True)

    @classmethod
    def get(cls, provider, remote_id):
        return DBSession.query(cls).filter_by(
            provider=provider,
            remote_id=str(remote_id),
        ).first()

