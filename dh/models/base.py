from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
)

from zope.sqlalchemy import ZopeTransactionExtension


class Base(object):
    EXCLUDE = ()
    INCLUDE = ()

    def _get_columns_names(self):
        return [c.name for c in self.__table__.columns]

    def _get_columns(self, exclude=None):
        if not exclude:
            exclude = ()

        return {
            col: getattr(self, col)
            for col in self._get_columns_names()
            if col not in exclude
        }

    def _get_relations(self):
        result = {}
        for rel in self.INCLUDE:
            result[rel] = getattr(self, rel).to_dict()
        return result

    def to_dict(self, exclude=None):
        exclude = list(self.EXCLUDE)
        exclude.extend(exclude or [])
        columns = self._get_columns(exclude=exclude)

        relations = self._get_relations()
        columns.update(relations)

        return columns

DBSession_ = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))

class SessionWrapper(object):
    def __getattribute__(self, attr):
        return DBSession_.__getattribute__(attr)


DBSession = SessionWrapper()

Base = declarative_base(cls=Base)