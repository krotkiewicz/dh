import os
import ConfigParser

from pyramid.config import Configurator
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid_beaker import session_factory_from_settings
from sqlalchemy import engine_from_config

# GLOBAL:
settings = {}

def expandvars_dict(settings):
    """Expands all environment variables in a settings dictionary."""
    return {
        key: os.path.expandvars(value)
        for key, value in settings.iteritems() if isinstance(value, basestring)
    }


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application. """
    file_ = global_config['__file__']
    here = global_config['here']

    prepare_db(**settings)
    from dh.utils.celery import prepare_celery
    prepare_celery(**settings)
    prepare_settings(file_, here)
    config = prepare_config(**settings)
    config.scan(
        ignore=['dh.testing', 'dh.tests'],
    )
    app =  config.make_wsgi_app()
    return app

def prepare_settings(file_, here):
    """
    Parse [settings] section in config file
    and save it to global settings variable.
    """
    parser = ConfigParser.SafeConfigParser({'here': here})
    # make parser case sensitive:
    parser.optionxform = str
    parser.read(file_)

    global settings
    for k, v in parser.items('settings'):
        settings[k] = v

    settings.pop('here')
    settings = expandvars_dict(settings)

    # some special options:

    settings['DEBUG'] = True
    if parser.has_option('settings', 'DEBUG'):
        settings['DEBUG'] = parser.getboolean('settings', 'DEBUG')


def prepare_db(**settings):
    from dh.models.base import DBSession, Base
    settings = expandvars_dict(settings)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine


def prepare_config(**settings):
    from dh.utils.auth import RootFactory, CustomAuthenticationPolicy
    from dh.utils.request import Request
    from dh.utils.renderers import JSON
    settings = expandvars_dict(settings)

    session_factory = session_factory_from_settings(settings)
    authn_policy = CustomAuthenticationPolicy('a')
    authz_policy = ACLAuthorizationPolicy()
    config = Configurator(
        settings=settings,
        authentication_policy=authn_policy,
        authorization_policy=authz_policy,
        root_factory=RootFactory,
        session_factory=session_factory,
        request_factory=Request,
    )
    config.add_renderer('json', JSON())

    config.add_static_view('static', 'static', cache_max_age=3600)
    current_dir = os.path.dirname(os.path.realpath(__file__))
    docs_path = current_dir + '/../../../docs/build/html'
    config.add_static_view(name='docs', path=docs_path)

    add_apis(config)
    add_views(config)
    return config


def add_views(config):
    config.add_route('home', '/')


def add_apis(config):
    config.add_route('create_user', '/v1/users')

    config.add_route('email_auth', '/v1/auth/email')
    config.add_route('facebook_auth', '/v1/auth/facebook')
    config.add_route('google_auth', '/v1/auth/google')

    config.add_route('activate_user', '/v1/activate/{activation_code}')
    config.add_route('access_token', '/v1/auth/access_token')

    config.add_route('login', '/v1/login')

    config.add_route('me', '/v1/me')
    config.add_route('emails', '/v1/me/emails')

    # change primary email ?
    config.add_route('debt_plus', '/v1/debt/plus')
    config.add_route('debt_minus', '/v1/debt/minus')
    config.add_route('debt_status', '/v1/debt/status')
    config.add_route('debt_history', '/v1/debt')
    config.add_route('debt_accept', '/v1/debt/{id}/accept')
    # reject debt ?
