import json
from functools import wraps

import formencode
from formencode.api import Invalid
from formencode.variabledecode import variable_decode
from pyramid.httpexceptions import HTTPBadRequest

from dh import settings


class Empty(formencode.Schema):
    pass


class Schema(object):
    def __init__(self, schema_in=None, out=None):
        self.schema_in = schema_in or Empty
        self.schema_out = out or Empty

    def __call__(self, method):
        @wraps(method)
        def wrap(wrapped_self):
            request = wrapped_self.request
            data = self.get_combined_data(request)
            data = self.validify(data)
            result = method(wrapped_self, data)
            debug = settings['DEBUG']
            if debug and self.schema_out and isinstance(result, dict):
                # validating outgress data only in debug mode
                # (devs env and tests)
                self.schema_out.from_python(result)
            return result

        wrap._schema_in = self.schema_in
        wrap._schema_out = self.schema_out
        return wrap

    def get_combined_data(self, request):
        """
        A little bit magic here.
        Covert all data that can be passed to the view to one dict.
        There are 3 data sources:
        1. request body (always json in our case)
        2. REST ids in path i.e. /users/:id will give us {'id': 1}
        3. request query string - ?query=abc will give us {'query': 'abc'}
        """
        body = {}
        if request.body:
            body = request.json
        url_param = request.matchdict
        query_params = variable_decode(request.GET)
        return dict(body.items() + url_param.items() + query_params.items())

    def validify(self, data):
        """
        Validate and convert data against self.schema.
        If validation doesn't success, raise formencode.api.Invalid error.
        """
        return self.schema_in.to_python(data)


schema = Schema
