from pyramid import request
from pyramid.decorator import reify
from pyramid.security import authenticated_userid

from dh.models.base import DBSession
from dh.models.user import User

class Request(request.Request):
    def __init__(self, *args, **kwargs):
        #shortcut:
        self.DB = DBSession
        super(Request, self).__init__(*args, **kwargs)

    @reify
    def user(self):
        user_id = authenticated_userid(self)
        if user_id:
            return self.DB.query(User).filter(User.id==user_id).one()

