class EmailMessage(object):
    """Representation of an e-mail message.

    Most probably you don't want to use this directly - check out
    .templates instead.
    """
    def __init__(
        self,
        template_name,
        user_email,
        user_name='',
        variables=None,
        tags=None,
    ):
        self.user_name = user_name or ''
        self.user_email = user_email
        self.template_name = template_name
        if tags is None:
            tags = []
        self.tags = tags[:]
        if not variables:
            variables = {}
        elif not isinstance(variables, dict):
            raise ValueError('variables must be provided as a dict')
        self.variables = variables

    def to_mandrill_api_message(self):
        """Converts message to dict that can be used with Mandrill

        Template content will be updated by following variables:
        - user name
        - user e-mail
        """
        variables = [
            {
                'name': 'user_name',
                'content': self.user_name,
            },
            {
                'name': 'user_email',
                'content': self.user_email
            },
        ]
        variables.extend([
            {'name': name, 'content': content}
            for name, content in self.variables.iteritems()
        ])
        converted = {
            'template_name': self.template_name,
            'template_content': [],  # must be provided, even if it's empty!
            'message': {
                'tags': self.tags,
                'global_merge_vars': variables,
                'to': [{
                    'email': self.user_email,
                    'name': self.user_name,
                }],
            },
        }
        return converted
