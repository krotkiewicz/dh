from .base import EmailMessage


class BaseTemplate(EmailMessage):
    """Wrapper for EmailMessage

    Will automatically set and validate all fields related to templates,
    and prepare nice EmailMessage object.
    """
    NAME = None
    TAGS = ()
    REQUIRED_VARIABLES = ()

    def __init__(self, user_email, user_name='', **variables):
        """Sets and passes all required fields to EmailMessage"""
        for variable in self.REQUIRED_VARIABLES:
            if variable not in variables:
                raise ValueError('Required variable %s not found' % variable)

        super(BaseTemplate, self).__init__(
            self.NAME,
            user_email,
            user_name=user_name,
            variables=variables,
            tags=self.TAGS,
        )


class ActivationEmail(BaseTemplate):
    NAME = 'activation-email'
    REQUIRED_VARIABLES = (
        'activation_url',
    )
