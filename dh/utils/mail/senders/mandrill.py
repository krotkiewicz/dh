import json
import logging

from mailsnake import MailSnake

from dh import settings
from dh.utils.celery import celery


def _get_api():
    mailsnake = MailSnake(
        apikey=settings['MANDRILL_API_KEY'],
        api='mandrill'
    )
    return mailsnake

@celery.task
def send(message):
    converted = message.to_mandrill_api_message()
    if settings['DEBUG']:
        msg = 'Development server detected - mail will NOT be sent'
        logging.info(msg)
        logging.info('Converted message:')
        logging.info('\n%s' % json.dumps(converted, sort_keys=True, indent=4))
        return
    api = _get_api()
    api.messages.send_template(**converted)
