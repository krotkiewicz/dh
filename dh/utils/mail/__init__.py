from .senders import mandrill
from . import templates


__all__ = [
    'send',
    'templates',
]

def send(message):
    mandrill.send.delay(message)
