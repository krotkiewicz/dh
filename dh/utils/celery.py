from __future__ import absolute_import
from celery import Celery

# Celery app, it will be replaced during app startup,
# it is instantiated here only IDE purpouses (jump to definition etc.)
celery = Celery()


def prepare_celery(**settings):
    global celery
    celery = Celery(
        'dh',
        broker=settings['amqp.url'],
        backend=settings['amqp.url'],
    )
