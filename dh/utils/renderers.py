from jsondate import dumps
from pyramid.renderers import JSON

class JSON(JSON):
    
    def __init__(self, *args, **kwargs):
        kwargs['serializer'] = dumps
        super(JSON, self).__init__(*args, **kwargs)