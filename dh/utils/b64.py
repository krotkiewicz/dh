from base64 import urlsafe_b64encode, urlsafe_b64decode


def encode(string):
    return urlsafe_b64encode(string).strip('=')

def decode(string):
    return urlsafe_b64decode(string + '=' * (4 - len(string) % 4))
