from pyramid.httpexceptions import (
    HTTPException, HTTPMethodNotAllowed, HTTPBadRequest, HTTPNotFound,
)

from dh.models.base import DBSession

class BaseAPI(object):

    http_method_names = ('get', 'post', 'put', 'delete')

    def __init__(self, context, request):
        self.context = context
        self.request = request
        # shortcut:
        self.DB = DBSession

    def dispatch(self):
        if self.request.method.lower() in self.http_method_names:
            handler = getattr(
                self,
                self.request.method.lower(),
                self.http_not_allowed_method,
            )
        else:
            handler = self.http_not_allowed_method
        return handler()

    def http_not_allowed_method(self):
        raise HTTPMethodNotAllowed("Method Not Allowed")

    def __call__(self):
        return self.dispatch()