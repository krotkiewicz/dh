from dh import settings


def get_absolute_url(path):
    server_address = settings['SERVER_ADDRESS']
    return '%s%s' % (server_address, path)
