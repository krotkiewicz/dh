
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.interfaces import IAuthenticationPolicy
from pyramid.security import Allow, Authenticated, ALL_PERMISSIONS, Everyone
from zope.interface import implementer

from dh.models.user import User, Token, Auth
from dh.utils import b64

class RootFactory(object):
    __acl__ = [
        (Allow, Everyone, 'view'),
        (Allow, Authenticated, 'auth')
    ]

    def __init__(self, request):
        self.request = request

@implementer(IAuthenticationPolicy)
class CustomAuthenticationPolicy(AuthTktAuthenticationPolicy):
    def unauthenticated_userid(self, request):
        sself = super(CustomAuthenticationPolicy, self)
        user_id = sself.unauthenticated_userid(request)
        if user_id is not None:
            return user_id
        DB = request.DB
        try:
            access_token = request.headers.get('X-AuthToken', '')
            user_id, scopes, expires = Auth.get_user_from_access_token(
                access_token,
            )
            return user_id
        except:
            return

