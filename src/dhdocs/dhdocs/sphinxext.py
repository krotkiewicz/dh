import sys
import docutils
from docutils.parsers.rst import Directive
from docutils import nodes, core
from itertools import izip_longest

import formencode
from pyramid.config import Configurator
from pyramid.interfaces import IRoutesMapper

import dh
from dh import add_apis


class FakeConfig(object):
    def __init__(self):
        self.views = {}

    def with_package(self, *arg, **kwargs):
        return self

    def add_subscriber(self, *arg, **kwargs):
        return self

    def add_view(self, *args, **kwargs):
        route_name = kwargs.get('route_name')
        view = kwargs['view']
        self.views[route_name] = view


class Helper(object):
    def __init__(self):
        config = Configurator()
        add_apis(config)
        config.scan()
        app = config.make_wsgi_app()
        mapper = app.registry.queryUtility(IRoutesMapper)
        self.routes = mapper.routes


        config = Configurator()
        fake_config = FakeConfig()
        scanner = config.venusian.Scanner(config=fake_config)
        scanner.scan(dh)

        self.views = fake_config.views

    def get_views(self):
        result = {}
        for name, route in self.routes.iteritems():
            result[name] = (route, self.views[name])
        return result

def nl(s):
    if not s.endswith('\n'):
        s += '\n'
    return s

class Env(object):
    temp_data = {}
    docname = ''


def rst2node(data):
    """Converts a reStructuredText into its node
    """
    if not data:
        return
    parser = docutils.parsers.rst.Parser()
    document = docutils.utils.new_document('<>')
    document.settings = docutils.frontend.OptionParser().get_default_values()
    document.settings.tab_width = 4
    document.settings.pep_references = False
    document.settings.rfc_references = False
    document.settings.env = Env()
    parser.parse(data, document)
    if len(document.children) == 1:
        return document.children[0]
    else:
        par = docutils.nodes.paragraph()
        for child in document.children:
            par += child
        return par


def trim(docstring):
    """
    Remove the tabs to spaces, and remove the extra spaces / tabs that are in
    front of the text in docstrings.

    Implementation taken from http://www.python.org/dev/peps/pep-0257/
    """
    if not docstring:
        return ''
    # Convert tabs to spaces (following the normal Python rules)
    # and split into a list of lines:
    lines = docstring.expandtabs().splitlines()
    # Determine minimum indentation (first line doesn't count):
    indent = sys.maxsize
    for line in lines[1:]:
        stripped = line.lstrip()
        if stripped:
            indent = min(indent, len(line) - len(stripped))
    # Remove indentation (first line is special):
    trimmed = [lines[0].strip()]
    if indent < sys.maxsize:
        for line in lines[1:]:
            trimmed.append(line[indent:].rstrip())
    # Strip off trailing and leading blank lines:
    while trimmed and not trimmed[-1]:
        trimmed.pop()
    while trimmed and not trimmed[0]:
        trimmed.pop(0)
    # Return a single string:
    res = '\n'.join(trimmed)
    res = res.decode('utf8')
    return res


class Table(object):
    def __init__(self, table):
        self.cols_width = [
            self._col_width(col)
            for col in zip(*table)
        ]

        self.header = table[0]
        self.grid = table[1:]

        self.num_cols = len(self.grid[0])

    def _row_height(self, row):
        return max(len(cell.split('\n')) for cell in row)

    def _col_width(self, col):
        cells = [cel for cells in col for cel in cells.split('\n')]
        return max([len(cell) for cell in cells]) + 2

    def line(self, linechar='-'):
        line = ''.join('+' + colsize*linechar for colsize in self.cols_width)
        return line + '+\n'

    def row(self, row):
        rowtpl = '| %s|\n'

        cells = [cell.split('\n') for cell in row]
        subrows = izip_longest(*cells, fillvalue='')

        result = []
        for subrow in subrows:
            middle = '| '.join([
                self.normalize_cell(i, x)
                for i, x in enumerate(subrow)
            ])
            result.append(rowtpl % middle)

        return ''.join(result)

    def row_line(self):
        return self.line()

    def header_line(self):
        return self.line(linechar='=')

    def normalize_cell(self, coln, string):
        col = self.cols_width[coln]
        return string + ((col - 1 - len(string)) * ' ')

    def make(self):
        rst = self.row_line()
        rst += '| ' + '| '.join([self.normalize_cell(i, x) for i, x in enumerate(self.header)]) + '|\n'
        rst += self.header_line()
        for row in self.grid:
            rst += self.row(row)
            rst += self.row_line()
        return rst


class FieldWrapper(object):
    def __init__(self, name, field):
        self._name = name
        self._field = field

    @property
    def name(self):
        name = '| **%s**' % self._name
        if self.required:
            name += '\n'
            name += '|   *required*'

        return name

    @property
    def required(self):
        min_ = getattr(self._field, 'min', None)
        if self._field.not_empty or min_:
            return True
        return False

    @property
    def type(self):
        if isinstance(self._field, formencode.ForEach):
            validators = self._field.validators
            validator = validators[0]
            name = validator.__name__
            id_ = 'object-%s' % name
            result = 'List of %s' % name
        elif isinstance(self._field, formencode.Schema):
            name = self._field.__class__.__name__
            id_ = 'object-%s' % name
            result = name
        else:
            result = self._field.__class__.__name__
        return result

    @property
    def desc(self):
        desc = ''
        min_ = getattr(self._field, 'min', None)
        max_ = getattr(self._field, 'max', None)
        if min_:
            desc += '\n'
            desc += '|   min length: %s' % min_
        if max_:
            desc += '\n'
            desc += '|   max length: %s' % max_

        return desc


class RenderFields(object):
    def __init__(self, fields):
        self.fields = fields

    def _field_to_row(self, name, field):
        fw = FieldWrapper(name, field)
        return [fw.name, fw.type, fw.desc]

    def render(self):
        table = [['Property', 'Type', 'Description']]
        for field_name, field in self.fields.iteritems():
            table.append(self._field_to_row(field_name, field))

        node = rst2node(Table(table).make())
        return node


class EndpointsDirective(Directive):
    required_arguments = 2
    has_content = True
    option_spec = {
        'name': str,
    }
    domain = 'dh'

    def __init__(self, *args, **kwargs):
        self.views = Helper().get_views()
        super(EndpointsDirective, self).__init__(*args, **kwargs)

    def _render_method(self, route, view, method):
        id_ = '%s-%s' % (route.name, method)
        method_callable = getattr(view, method)
        schema_in = getattr(method_callable, '_schema_in', None)
        schema_out = getattr(method_callable, '_schema_out', None)
        section = nodes.section(ids=[id_])
        title = '%s %s' % (method.upper(), route.pattern)
        section += nodes.title(text=title)
        if method_callable.__doc__:
            desc = trim(method_callable.__doc__)
            desc = '| ' + desc.replace('\n', '\n| ')
            definition = rst2node('**Description:**\n\n' + desc)
            section += definition

        title = rst2node('**Request:**\n')
        section += title
        if schema_in and schema_in.fields:
            section += (RenderFields(schema_in.fields).render())
        else:
            section += rst2node('  No request params')
        title = rst2node('**Response:**\n')
        section += title
        if schema_out and schema_out.fields:
            section += (RenderFields(schema_out.fields).render())
        else:
            section += rst2node('  Response body empty')

        return section

    def run(self):
        method, route_name = self.arguments
        method = method.lower()
        route, view = self.views[route_name]

        return [self._render_method(route, view, method)]


class ObjectsDirective(Directive):
    has_content = True
    domain = 'dh'

    obj_schemas = set()

    def __init__(self, *args, **kwargs):
        views = Helper().get_views()
        def get_schemas(view):
            schemas = []
            for method in ('get', 'post', 'put', 'patch', 'delete'):
                if hasattr(view, method):
                    method_m = getattr(view, method)
                    if getattr(method_m, '_schema_in', None):
                        schemas.append(getattr(method_m, '_schema_in'))
                    if getattr(method_m, '_schema_out', None):
                        schemas.append(getattr(method_m, '_schema_out'))
            return schemas
        if self.obj_schemas:
            return

        schemas = [
            schema
            for name, (route, view) in views.items()
            for schema in get_schemas(view)
        ]

        def check_fields(fields):
            for field_schema in fields:
                if isinstance(field_schema, formencode.ForEach):
                    validators = field_schema.validators
                    validator = validators[0]
                    self.obj_schemas.add(validator)
                    check_fields(validator.fields.values())
                elif isinstance(field_schema, formencode.Schema):
                    self.obj_schemas.add(field_schema.__class__)
                    check_fields(field_schema.fields.values())


        for schema in schemas:
            check_fields(schema.fields.values())

        super(ObjectsDirective, self).__init__(*args, **kwargs)

    def _render_obj(self, obj):
        id_ = 'object-%s' % obj.__name__
        section = nodes.section(ids=[id_])
        section += nodes.title(text=obj.__name__)

        section += RenderFields(obj.fields).render()
        return section

    def run(self):
        result = []
        objects = sorted(
            self.obj_schemas,
            key=lambda o: o.__name__
        )
        for obj in objects:
            result.append(self._render_obj(obj))
        return result


def setup(app):
    app.add_directive('dh-endpoint', EndpointsDirective)
    app.add_directive('dh-objects', ObjectsDirective)

