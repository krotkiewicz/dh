import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
]


setup(name='dh',
      version='0.0',
      description='dh',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='dh',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = dh:main
      [console_scripts]
      initialize_dh_db = dh.scripts.initializedb:main
      runserver = dh.scripts.runserver:main
      celeryworker = dh.scripts.celery:main
      """,
      )
