=======================================
Debt API
=======================================

.. dh-endpoint:: POST debt_plus
.. dh-endpoint:: POST debt_minus
.. dh-endpoint:: GET debt_status
.. dh-endpoint:: GET debt_history
.. dh-endpoint:: POST debt_accept
