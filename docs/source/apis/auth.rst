=======================================
Auth API
=======================================

.. dh-endpoint:: POST create_user
.. dh-endpoint:: GET activate_user
.. dh-endpoint:: GET access_token
.. dh-endpoint:: GET refresh_token
