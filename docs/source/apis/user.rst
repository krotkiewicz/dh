=======================================
User API
=======================================

.. dh-endpoint:: GET me
.. dh-endpoint:: GET emails
.. dh-endpoint:: POST emails
